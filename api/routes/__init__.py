# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later

from flask import Blueprint
from flask_restful import Api

api_bp = Blueprint('api', __name__)
rest = Api(api_bp)

from .auth import Authenitcation
from .users import Users, User, AuthUser
from .invites import Invites, Invite
from .forms import Forms
# from .forms import Forms, Form

from .admin_utils import TestSMTP
