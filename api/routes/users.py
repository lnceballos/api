# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later

from flask import request
from flask_jwt_extended import jwt_required, get_current_user
from flask_restful import Resource
from marshmallow import ValidationError

# TODO consider wrapping alchemy expcetions
from sqlalchemy.exc import IntegrityError

from api import db
from api.routes import rest, api_bp
from api.models.user import User as UserModel
from api.routes.auth import user_has_roles

from .schemas.user import UserSchema
from .schemas.query import CollectionQuerySchema
from .schemas.output import MetaSchema

user_schema = UserSchema()
users_schema = UserSchema(many=True)
collection_query_schema = CollectionQuerySchema()
meta_schema = MetaSchema()

class Users(Resource):
    @user_has_roles(['admin'])
    def get(self):
        query = collection_query_schema.load(request.args)
        users = UserModel.query.paginate(query['page'], query['size'])
        items = users_schema.dump(users.items)
        meta = meta_schema.dump({'count': int(users.total)})
        return {
            'items': items,
            'meta': meta
        }, 200
    @user_has_roles(['admin'])
    def post(self):
        try:
            json = user_schema.load(request.get_json())
        except ValidationError as err:
            return err.messages, 422

        current_user = get_current_user()
        user = UserModel(
            createdBy=current_user['id'],
            username=json['username'],
            email=json['email'],
            group=json['group']
        )
        user.set_password(json['password'])
        try:
            db.session.add(user)
            db.session.commit()
            return {'id': user.id }, 201
        except IntegrityError as exception_message:
            return {
                'message': str(exception_message)
            }, 400


class User(Resource):
    @user_has_roles(['admin'])
    def get(self, id):
        user = UserModel.query.filter_by(id=id).first()
        res = user_schema.dump(user)
        return res, 200

class AuthUser(Resource):
    @jwt_required
    def get(self):
        loggedUser = get_current_user()
        user = UserModel.query.filter_by(id=loggedUser['id']).first()
        res = user_schema.dump(user)
        return res, 200


@api_bp.route('/users/exists', methods=['POST'])
def username_exists():
    json = request.get_json()
    username = json['username']
    if not username:
        return {
            'message': 'Username is required'
        }, 422

    user = UserModel.query.filter_by(username=username).first()
    if user:
        return {
            'exists': True
        }, 200
    return {
        'exists': False
    }, 200


rest.add_resource(Users, '/users')
rest.add_resource(AuthUser, '/users/me')
rest.add_resource(User, '/users/<id>')
