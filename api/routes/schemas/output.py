# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later

from marshmallow import Schema, fields, EXCLUDE


class MetaSchema(Schema):
    class Meta:
        # EXCLUDE: unknown fields will dropped (not be parsed)
        unknown = EXCLUDE
    count = fields.Int(dump_only=True)
