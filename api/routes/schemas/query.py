# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later

from marshmallow import Schema, fields


class CollectionQuerySchema(Schema):
    # missing specifies the default deserialization value for a field
    size = fields.Int(missing=10)
    page = fields.Int(missing=1)
