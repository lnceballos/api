# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later

from marshmallow import Schema, fields
class ResetPasswordSchema(Schema):
    password = fields.Str(load_only=True, required=True)
    reset_token = fields.Str(required=True)
