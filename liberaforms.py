# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later

import os
from api import create_app, db
from flask_migrate import Migrate
from commands import register_commands

app = create_app(os.environ["FLASK_CONFIG"] or "default")
migrate = Migrate(app, db)
register_commands(app)

@app.shell_context_processor
def make_shell_context():
    return dict(app=app, db=db)
